# scala-seed-project

## Text size of code in IntelliJ:  
Using this font and size in IntelliJ on 4k monitor
Settings -> Editor -> Font  
Ubuntu Mono, 34

## creating an sbt project
[Help](https://docs.scala-lang.org/getting-started-sbt-track/getting-started-with-scala-and-sbt-on-the-command-line.html)  
*New scala project:*  

```bash
sbt new scala/hello-world.g8
sbt
run
```

## creating an sbt project + unit testing support
[Help](https://docs.scala-lang.org/getting-started-sbt-track/testing-scala-with-sbt-on-the-command-line.html)  

```bash
sbt new scala/scalatest-example.g8
sbt
test
```

https://www.scalacheck.org/
https://etorreborre.github.io/specs2/

## auto-format code using scalafmt

### IntelliJ IDEA

[Help](https://scalameta.org/scalafmt/docs/installation.html)  
copy a good `.scalafmt.conf` file from someone  
https://scalameta.org/scalafmt/docs/configuration.html  
Preferences > Editor > Code Style > Scala -> Reformat on file save

### CLI:

*add to build.sbt:*  
  
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.0.0")  

*Install Coursier:*  

```bash
curl -Lo coursier https://git.io/coursier-cli &&
    chmod +x coursier
```

*Generate `scalafmt` command*:

```bash
./coursier bootstrap org.scalameta:scalafmt-cli_2.12:2.0.0 \
  -r sonatype:snapshots \
  -o scalafmt --main org.scalafmt.cli.Cli

./scalafmt --version
```

## static code analysis using `scalastyle`

[Help](http://www.scalastyle.org/sbt.html)  

*add to build.sbt:*
  
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")

```bash
sbt scalastyleGenerateConfig  
sbt scalastyle
```

Edit `./scalastyle-config.xml` to customize the rules.  

## Commit as per conventions using `Git Commit Template` plugin

[Help](https://plugins.jetbrains.com/plugin/9861-git-commit-template)  

Settings -> Plugins -> search and install `Git Commit Template`

Click `Create Commit Message` button when committing(VCS -> Commit... or Ctrl-K) and fill the details.

## Commitizen
[Help](https://github.com/commitizen/cz-cli#making-your-repo-commitizen-friendly)  
```bash
npm i -g yarn  
yarn init -y  
yarn add husky --dev  
```

*To add commitizen globally*:  
\# npm i -g commitizen (not sure why yarn global add doesn't create global commands)  
\# commitizen init cz-conventional-changelog --yarn --dev --exact  

*To add commitizen locally*:  
```bash
yarn add commitizen --dev  
yarn commitizen init cz-conventional-changelog --yarn --dev --exact  
```

*To commit*:
yarn git-cz

*Or create a commit job*:

```text
"scripts": {
    "commit": "git-cz"
  }
```
```bash
yarn commit
```

*We can commit normally after integrating commitizen in husky hooks*:

```text
{
  "husky": {
    "hooks": {
      "prepare-commit-msg": "exec < /dev/tty && yarn git-cz --hook",
    }
  }
}
```
```bash
git commit
```

## Add support to lint commit messages

[Help](https://github.com/commitizen/cz-cli)

*Install `commitlint`*:
```bash
yarn add --dev @commitlint/{config-conventional,cli}
echo "module.exports = {extends: ['@commitlint/config-conventional']}" > commitlint.config.js
```

*Add hook to husky config*:

```text
{
  "husky": {
    "hooks": {
      "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
    }  
  }
}
```
